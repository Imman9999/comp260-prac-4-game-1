﻿using UnityEngine;
using System.Collections;

public class BeeSpawner : MonoBehaviour {

	// Use this for initialization
	public BeeMove beePrefab;
	public int nBees = 50;

	public float xMin, yMin;
	public float width, height;

	void Start () {

		for (int i = 0; i < nBees; i++) {
			BeeMove Bee = Instantiate (beePrefab);

			Bee.transform.parent = transform;
			Bee.gameObject.name = "Bee " + i;


			float x = xMin + Random.value * width;
			float y = yMin + Random.value * height;
			Bee.transform.position = new Vector2 (x, y);
	
		}

		PlayerMove player = FindObjectOfType<PlayerMove>();
		TargetJoint2D = player.transform;





	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
