﻿using UnityEngine;
using System.Collections;

public class Player2Move : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

	public Vector2 velocity;
	public float maxSpeed = 5.0f;

	// Update is called once per frame
	void Update () {
		Vector2 direction;
		direction.x = Input.GetAxis ("HorizontalTwo");
		direction.y = Input.GetAxis ("VerticalTwo");

		Vector2 velocity = direction * maxSpeed;
		transform.Translate (velocity * Time.deltaTime);

	}
}
